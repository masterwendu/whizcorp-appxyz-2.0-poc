# AppZYZ 2.0 POC
[![Build Status](https://ci.wendelin.dev/api/badges/masterwendu/whizcorp-appxyz-2.0-poc/status.svg?ref=refs/heads/main)](https://ci.wendelin.dev/masterwendu/whizcorp-appxyz-2.0-poc)

## Development
### .env file
Create a `.env` file based on `.env_example` to connect your app to the database.

This file is used by the postgres docker db and the java application. It defines 3 variables: 

- `POSTGRES_USER`
- `POSTGRES_PASSWORD`
- `APP_DATABASE` = The databasename which this application uses

**`APP_DATABASE` is not filled in automatically to the file `initDevDb.sql` you have to set the correct database name before your initial postgres start**


### PG Database

To test the application you have to first start the DB via docker compose:
```zsh
docker compose up -d
```
The database is created on init of the db.

### Spring boot application
Create a `.env` file based on `.env_example` to connect your app to the database.

You need JDK in version **17** installed and set in your JAVA_HOME variable of your system.

Run 
```zsh
./gradlew bootRun
```
to start the REST server.

To quickly test the endpoints you can use VS-Code with extension [Thunderclient](https://www.thunderclient.com/) installed and import the file `thunder-collection_appxyz.json`.
Or you use [postman](https://www.postman.com/) and import the file `postman-collection_appxyz.json`.

