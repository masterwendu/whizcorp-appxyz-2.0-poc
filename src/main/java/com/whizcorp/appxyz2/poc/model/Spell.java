package com.whizcorp.appxyz2.poc.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "spell")
public class Spell {
  @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "incantation", nullable = false)
	private String incantation;

	@Column(name = "magic_outcome", nullable = false)
	private String magicOutcome;

	public Spell() {
	}

	public Spell(String incantation, String magicOutcome) {
		this.incantation = incantation;
		this.magicOutcome = magicOutcome;
	}

	public long getId() {
		return id;
	}

	public String getIncantation() {
		return incantation;
	}

	public void setIncantation(String incantation) {
		this.incantation = incantation;
	}

	public String getMagicOutcome() {
		return magicOutcome;
	}

	public void setMagicOutcome(String magicOutcome) {
		this.magicOutcome = magicOutcome;
	}

	@Override
	public String toString() {
		return String.format("Spell [id=%d, incantation='%s', magicOutcome='%s']", id, incantation, magicOutcome);
	}
}
