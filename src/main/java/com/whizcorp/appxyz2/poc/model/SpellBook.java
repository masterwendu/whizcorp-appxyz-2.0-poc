package com.whizcorp.appxyz2.poc.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "spell_book")
public class SpellBook {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "wizard_name", nullable = false)
  private String wizardName;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "spell_book_id")
  private List<Spell> spells = new ArrayList<>();

  public SpellBook() {
  }

  public SpellBook(String wizardName) {
    this.wizardName = wizardName;
  }

  public long getId() {
    return id;
  }

  public String getWizardName() {
    return wizardName;
  }

  public void setWizardName(String wizardName) {
    this.wizardName = wizardName;
  }

  public List<Spell> getSpells() {
    return spells;
  }

  public void addSpell(Spell spell) {
    spells.add(spell);
  }

  public void removeSpell(Spell spell) {
    spells.remove(spell);
  }

  public int getNumberOfSpells() {
    return spells.size();
  }

  @Override
  public String toString() {
    return String.format("SpellBook [id=%d, Number of Spells=%d]", id, this.getNumberOfSpells());
  }
}
