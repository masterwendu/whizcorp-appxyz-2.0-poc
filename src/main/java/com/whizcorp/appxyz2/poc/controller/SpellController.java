package com.whizcorp.appxyz2.poc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.whizcorp.appxyz2.poc.model.Spell;
import com.whizcorp.appxyz2.poc.repository.SpellBookRepository;
import com.whizcorp.appxyz2.poc.repository.SpellRepository;

@RestController
public class SpellController {
  
  @Autowired
  private SpellBookRepository spellBookRepository;

  @Autowired
  private SpellRepository spellRepository;

  @PostMapping("/spellbook/{id}/spell")
  public ResponseEntity<Spell> addSpellToSpellBook(@PathVariable(value = "id") Long id, @RequestParam(value = "incantation") String incantation, @RequestParam(value = "magicOutcome") String magicOutcome) {
    var spellBook = spellBookRepository.findById(id);
    if (spellBook.isPresent()) {
      var spell = new Spell(incantation, magicOutcome);
      spellBook.get().addSpell(spell);
      spellBookRepository.save(spellBook.get());
      return new ResponseEntity<>(spell, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping("/spell/{id}")
  public ResponseEntity<Spell> getSpell(@PathVariable(value = "id") Long id) {
    var spell = spellRepository.findById(id);
    if (spell.isPresent()) {
      return new ResponseEntity<>(spell.get(), HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping("/callspell/{incantation}")
  public ResponseEntity<String> callSpell(@PathVariable(value = "incantation") String incantation) {
    var spell = spellRepository.findByIncantation(incantation);
    if (spell.isPresent()) {
      return new ResponseEntity<>(spell.get().getMagicOutcome(), HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @PatchMapping("/spell/{id}")
  public ResponseEntity<Spell> updateSpell(@PathVariable(value = "id") Long id, @RequestParam(value = "incantation", required = false) String incantation, @RequestParam(value = "magicOutcome", required = false) String magicOutcome) {
    var spell = spellRepository.findById(id);
    if (spell.isPresent()) {
      if (incantation != null && !incantation.isEmpty()) {
        spell.get().setIncantation(incantation);
      }
      if (magicOutcome != null && !magicOutcome.isEmpty()){
        spell.get().setMagicOutcome(magicOutcome);
      }
      spellRepository.save(spell.get());
      return new ResponseEntity<>(spell.get(), HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @DeleteMapping("/spell/{id}")
  public ResponseEntity<?> deleteSpell(@PathVariable(value = "id") Long id) {
    var spellExists = spellRepository.existsById(id);
    if (spellExists) {
      spellRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

}
