package com.whizcorp.appxyz2.poc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.whizcorp.appxyz2.poc.model.SpellBook;
import com.whizcorp.appxyz2.poc.repository.SpellBookRepository;

@RestController
public class SpellBookController {

  @Autowired
  private SpellBookRepository spellBookRepository;

  @PostMapping("/spellbook")
  public ResponseEntity<SpellBook> createSpellBook(@RequestParam(value = "wizardName") String wizardName) {
    var spellBook = new SpellBook(wizardName);
    spellBookRepository.save(spellBook);
    return new ResponseEntity<>(spellBook, HttpStatus.CREATED);
  }

  @GetMapping("/spellbooks")
  public ResponseEntity<Iterable<SpellBook>> getSpellBooks() {
    var spellBooks = spellBookRepository.findAll();
    if (spellBooks.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    return new ResponseEntity<>(spellBooks, HttpStatus.OK);
  }

  @GetMapping("/spellbook/{id}")
  public ResponseEntity<SpellBook> getSpellBook(@PathVariable(value = "id") Long id) {
    var spellBook = spellBookRepository.findById(id);
    if (spellBook.isPresent()) {
      return new ResponseEntity<>(spellBook.get(), HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @PatchMapping("/spellbook/{id}")
  public ResponseEntity<SpellBook> updateSpellBookWizardName(@PathVariable(value = "id") Long id, @RequestParam(value = "wizardName") String wizardName) {
    var spellBook = spellBookRepository.findById(id);
    if (spellBook.isPresent()) {
      spellBook.get().setWizardName(wizardName);
      spellBookRepository.save(spellBook.get());
      return new ResponseEntity<>(spellBook.get(), HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @DeleteMapping("/spellbook/{id}")
  public ResponseEntity<?> deleteSpellBook(@PathVariable(value = "id") Long id) {
    var spellBookExists = spellBookRepository.existsById(id);
    if (spellBookExists) {
      spellBookRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }
}
