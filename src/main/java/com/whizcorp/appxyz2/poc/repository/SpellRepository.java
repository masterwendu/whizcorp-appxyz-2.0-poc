package com.whizcorp.appxyz2.poc.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.whizcorp.appxyz2.poc.model.Spell;

public interface SpellRepository extends JpaRepository<Spell, Long> {
  Optional<Spell> findByIncantation(String incantation);
  boolean existsById(Long id);
}
