package com.whizcorp.appxyz2.poc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.whizcorp.appxyz2.poc.model.SpellBook;

public interface SpellBookRepository extends JpaRepository<SpellBook, Long> {
  boolean existsById(Long id);
}
